let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];
console.log(studentNumbers);

// [SECTION] Arrays - 
/*
       - Arrays are used to store multiple related values in a single variable.
       - They are declared using square brackets ([]) also known as "Array Literals"
       - Arrays it also provides access to a number of functions/methods that help in manipulation array.
           - Methods are used to manipulate information stored within the same object.
       - Array are also objects which is another data type.
       - The main difference of arrays with object is that it contains information in a form of "list" unlike objects which uses "properties" (key-value pair).
       - Syntax:
           let/const arrayName = [elementA, elementB, elementC, ... , ElementNth];
   */

   console.log("Examples of Array: ");

   		let grades = [97, 83, 21, 33];
   		let computerBrands = ["Acer", "Asus", "HP"];

   	console.log(grades);
   	console.log(computerBrands);


   	let mixedArr = ["John", "Doe", 12, false, null]; //not reco

   	console.log(mixedArr);


   	let myTasks = [
   		"drink html",
   		"eat javascript",
   		"inhale css",
   		"bake mongoDB"
   	];
   	console.log(myTasks);

// creating an array with values from variables

   	let city1 = "Tokyo";
   	let city2 = "Manila";
   	let city3 = "Jakarta";

   	let cities = [city1, city2, city3];
   	console.log(cities);
   	console.log("---------------");

 // .length property (Same with variable.length) << total numbers/elements in array/string


 console.log(myTasks.length);
 console.log(cities.length);
 console.log(studentNumbers.length);



console.log("-----------");

console.log("Using .length property for string size");

let word = "word";
console.log(word.length);

let name = "Chris Custodio";
console.log(name.length);

// Removing last element from an array
console.log("Remove the last element from an array");
myTasks.length = myTasks.length - 1;
console.log(myTasks); // drink html, eat js, inhale css

// or use pop() to remove
myTasks.pop();
console.log(myTasks); // drink html, eat js


// or use decrement to delete last element in an array
myTasks.length--;
console.log(myTasks); // drink html remains

// WE CANT USE beside .length, to delete elements/characters on a string!!!

console.log("-----------");

console.log("Add an element from an array");

let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles);

theBeatles[4] = "Cardo"; // OR theBeatles[theBeatles.length] = "Cardo";
console.log(theBeatles);


// [SECTION] Reading from Arrays
   /*
       - Accessing array elements is one of the common task that we do with an array.
       - This can be done through the use of array indexes.
       - Each element in an array is associated with it's own index number.
       - The first element in an array is associated with the number 0, and increasing this number by 1 for every element.
       - Array indexes it is actually refer to a memory address/location

       Array Address: 0x7ffe942bad0
       Array[0] = 0x7ffe942bad0
       Array[1] = 0x7ffe942bad4
       Array[2] = 0x7ffe942bad8

       - Syntax
           arrayName[index];
   */

   let lakersLegends=["Kobe", "Shaq", "Lebron", "Magic"];

   console.log(lakersLegends[1]);

   console.log(lakersLegends[3]);

console.log("-----------");

console.log("Change the last element of an array")

lakersLegends[lakersLegends.length-1] = "Westbrook"; // OR let lastElementIndex = lakersLegends.length-1;

console.log(lakersLegends); // OR console.log(lastElementIndex);
// console.log(lastElementIndex);

console.log("-----------");

console.log("Adding a new items into an array using indices");

let newArray = []; // if const BAWAL, need paisa isa
newArray = ["Dudong", "dadang"];

console.log(newArray[0]); //undefined output on console if no reassignment using CONS, but using LET, it is possible

newArray[0] = "Cloud Strife";

console.log(newArray);

newArray[1] = "Cloud Strike";

console.log(newArray);

console.log("-----------");



console.log("Add element in the end of an array using .lenght property"); // COMMON SENSE

newArray[newArray.length] = "Barret";
console.log(newArray);

for (let index = 0; index < newArray.length; index++){
	console.log(newArray[index]);
}



console.log("Filtering an array using loop and conditional statements: ");

let numArray = [5, 12, 30, 40, 53, 60];


for(let index=0; index < numArray.length; index++){
	if(numArray[index] % 5 == 0){
		console.log(numArray[index] + " is divisible by 5.");
	}
	else{
		console.log(numArray[index] + " is NOT divisible by 5.");
	}
}


console.log("-----------");
// MULTIDIMENSIONAL ARRAYS

/*
   -Multidimensional arrays are useful for storing complex data structures.
   - A practical application of this is to help visualize/create real world objects.
   - This is frequently used to store data for mathematic computations, image processing, and record management.
   - Array within an Array
*/

// Create chessboard
let chessBoard = [
   ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
   ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
   ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
   ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
   ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
   ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
   ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
   ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

console.table(chessBoard);

// access an element of a multi dimensional arrays
// syntax: multiArr[outerArr][innerArray];
					// row     // column
console.log(chessBoard[3][4]); //e4 ????????

console.log(chessBoard[2][5]); //f3 ????????